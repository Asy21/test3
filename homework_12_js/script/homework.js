"use strict"
let containerIMG = document.querySelector("div");

let btn1 = document.createElement("button");
let btn2 = document.createElement("button");
btn1.textContent = "Прекратить";
btn2.textContent = "Возобновить показ";
btn2.style.cssText = 'background-color: black; color: white; padding: 12px; border: none; border-radius: 20px'
btn1.style.cssText = 'margin-bottom:5px; background-color: black; color: white; padding: 12px; border: none; border-radius: 20px'
containerIMG.after(btn1, btn2);

let activeIMG = document.querySelector("img");
const myFunction = () => {
    let nextElem = activeIMG.nextElementSibling;
    if (!nextElem) {
        nextElem = document.querySelector("img");
    }
    activeIMG.classList.remove("active");
    nextElem.classList.add("active");
    activeIMG = nextElem;
};
let timer = setInterval(myFunction, 3000)

btn1.addEventListener("click", (event) => {
    clearInterval(timer);

});
btn2.addEventListener("click", (event) => {
    timer = setInterval(myFunction, 3000);
});



