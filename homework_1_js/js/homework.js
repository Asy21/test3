"use strict";

function runHomework1(defaultAge, defaultName) {
    let ageString = prompt("How old are you?", defaultAge);
    let nameString = prompt("What's your name", defaultName);
    let ageNumber = ageString === "" ? NaN : Number(ageString);

    console.log(`Age:${ageNumber}  Name:${nameString}`);


    if (isNaN(ageNumber)) {
        alert("You entered the wrong age, please try again");
        runHomework1(ageString, nameString);
    } else if (nameString == null || nameString == '') {
        alert("You didn't enter your name, please  try again");
        runHomework1(defaultAge, defaultName);
    } else if (Number(nameString)) {
        alert("Name should not be a number try again");
        runHomework1(defaultAge, nameString);
        }
    else if (ageNumber < 18) {
        alert("You are not allowed to visit this website.");
    } else if (ageNumber >= 23) {
        alert("Welcome, " + nameString);
    } else if (ageNumber >= 18 || ageNumber <= 22) {
        const result = confirm("Are you sure you want to continue?");
        if (result === true) {
            alert("Welcome, " + nameString);
        } else {
            alert("You are not allowed to visit this website.");
        }
    }


}
runHomework1("18", "Nastya");

