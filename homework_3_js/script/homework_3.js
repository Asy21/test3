"use strict"

let a;
let b;
let operator;

do {
    a = +prompt("Введите первое число", "5");
    b = +prompt("Введите второе число", "6");
    operator = prompt("Введите знак операции + - * /","+");
    const result = calcNumber(a, b, operator);
    if (!isNaN(result)) {
        console.log(result);
        break;
    }
} while (isNaN(a) || isNaN(b))

function calcNumber(a, b, operator) {
    switch (operator) {
        case "-":
            return a - b;
        case "*":
            return a * b;
        case "+":
            return a + b;
        case "/":
            return a / b;
        default:
            return NaN;
    }
}
