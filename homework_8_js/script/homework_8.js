"use strict"
const input = document.getElementById("inp");
const defaultBorderColor = input.style.borderColor;
const boxEl = document.getElementById("box");
const btn = document.createElement("button");
const defaultColor = input.style.color
btn.textContent = "X";

let span;

input.addEventListener("focusout", () => {
    input.style.borderColor = defaultBorderColor;

    input.style.color = "#00ff04";
    const val = document.getElementById("inp").value;

    if (span) {
        span.remove();
    }

    if (input.value < 0) {
        document.querySelector('[data-error]')?.remove();
        input.style.borderColor = "#ff2400";
        const error = document.createElement("div");
        error.append("Please enter correct price");
        error.setAttribute("data-error", "");
        document.body.append(error);

    } else {
        document.querySelector('[data-error]')?.remove();
        if (input.value !== "") {
            span = document.createElement("span");
            span.textContent = `Текущая цена:  ${val}`;
            span.append(btn);
            btn.style.cssText = "background: none; font-size: 1vw; margin-left: 7px";
            boxEl.prepend(span);
        }
    }

    btn.addEventListener("click", (e) => {
        span.remove();
        input.value = "";
    });
});

input.addEventListener("focus", () => {
    input.style.borderColor = "#00ff04";
    input.style.color = defaultColor;
});
