"use strict"
//второй вариант
// function conclusion(arr, parent = document.body) {
//     parent = document.createElement("ul");
//
//     arr.forEach(function (item, i, arr) {
//         let parent2 = document.createElement("li");
//         parent2.append(item);
//         parent.append(parent2);
//     });
//     document.body.append(parent);
// }
//
// conclusion(["hello", "world ", "Kiev", "Kharkiv", "Odessa", "Lviv"]);


function conclusion(arr, container = document.body) {
    let elements = arr.map(function (item) {
        return `<li> ${item} </li>`;
    });
    container.innerHTML = `<ul> ${elements.join("")} </ul>`;
}

conclusion(["hello", "world ", "Kiev", "Kharkiv", "Odessa", "Lviv"]);