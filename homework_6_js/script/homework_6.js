"use strict"
 const filterBy = (math, dataType) => {
     const result = math.filter(el => typeof el !== dataType);
     return result;
 }
 let resultOne = filterBy(['hello', 'world', 23, '23', null], "string");
 let resultTwo = filterBy(['hello', 'world', 23, '23', null], "number");
 console.log(resultOne);
 console.log(resultTwo);


