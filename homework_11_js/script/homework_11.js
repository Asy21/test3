"use strict"
document.addEventListener("keypress", (e) => {
  const documentElement = document.getElementById(e.key.toLowerCase());
  if (!documentElement) {
    alert(`No element for ${e.key}`);
    return;
  }
  document.querySelectorAll(".btn").forEach(el => el.style.backgroundColor = "#000000");
  documentElement.style.backgroundColor = "#FA8072";
})