"use strict"
let tabsElem = document.querySelector(".tabs-title");
let textEl = document.querySelector(".document_text");


textEl.classList.add("active");
tabsElem.classList.add("active");


function selectPanel(e) {
    let target = e.target.dataset.target;
    document.querySelectorAll(".document_text").forEach(el => el.classList.remove('active'));
    document.querySelectorAll(".tabs-title").forEach(el => el.classList.remove('active'));
    e.target.classList.add('active');
    document.querySelector(target).classList.add('active');
};

document.querySelectorAll('.tabs-title').forEach(el => {
    el.addEventListener('click', selectPanel)
});

