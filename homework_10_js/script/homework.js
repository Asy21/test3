"use strict"

document.body.querySelectorAll(".icon-password").forEach((element) => {
    element.addEventListener("click", (event) => {
        event.preventDefault();
        const parent = event.target.parentElement;
        const input = parent.querySelector("input");
        const openEye = parent.querySelector(".fa-eye.icon-password");
        const closedEye = parent.querySelector(".fa-eye-slash.icon-password");
        if (input.type === "password") {
            input.type = "text";
            openEye.style.display = "inline";
            closedEye.style.display = "none";
        } else {
            input.type = "password";
            openEye.style.display = "none";
            closedEye.style.display = "inline";
        }

    });
});

let btn = document.getElementById("button");
let x = document.createElement("p");
btn.addEventListener("click", (event) => {
    event.preventDefault();
    let inp1 = document.getElementById("first_inp").value;
    let inp2 = document.getElementById("second_inp").value;
    console.log(inp1);
    console.log(inp2);
    const parentDiv = btn.parentNode;
    if (inp1 !== inp2) {
        x.textContent = "Нужно ввести одинаковые значения";
        x.style.color = "#e34234";
        document.body.appendChild(x);
        parentDiv.insertBefore(x, btn);
    } else if (inp1 === inp2) {
        alert("You are welcome");
        x.remove();
    }
});
