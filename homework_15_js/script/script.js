"use strict"

function factorial() {
    let numb = +prompt("ваше число?")
    if (numb < 1 || isNaN(numb)) {
        return factorial();
    }
    for (let i = numb; i > 1; i--) {
        numb *= i - 1;
    }
    return numb;
}
console.log(factorial());